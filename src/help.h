#ifndef HELP_H
#define HELP_H

#define OPTION_HELP_SHORT "-h"
#define OPTION_HELP_LONG "--help"

void help_opt();
void help_short();
void help_full();

#endif
