#ifndef LM_STRING_H
#define LM_STRING_H

int str_eql(const char *c1, const char *c2);
int str_len(const char *s);
int a_to_i(const char *s);

#endif
